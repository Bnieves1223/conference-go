from unicodedata import name
from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference
from django.views.decorators.http import require_http_methods       
import json

from .models import Attendee
class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
 
    ]

    
class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]
    
@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method=="GET":   
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    if request.method=="GET":  
        attendees = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendees,
            encoder=AttendeeDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )