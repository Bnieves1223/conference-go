import requests
import json
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
        
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return content["photos"][0]["url"]
    except:
        return {"picture_url": None}

def get_weather_data(city, state):
    params = {
        "q": f'{city}, {state}, US',
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params,)
    content = json.loads(response.content)
    latitude = content[0]["lat"]
    longtitude = content[0]["lon"]
    
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longtitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    response = requests.get(url, params=params,)
    content = json.loads(response.content)
    return {
        "temperature": content['main']['temp'],
        "weather_description": content["weather"][0]["description"],
    }
        
